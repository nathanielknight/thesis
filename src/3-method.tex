\section{Method}
\label{method}
This study's aim is to combine the observations from the improve
network described in section \ref{improve network} with the GEOS-Chem
adjoint (sections \ref{geos chem model} and \ref{geos chem adjoint})
to create optimized emissions estimates for black and organic carbon
using the methods described in section \ref{adjoint method}. This
chapter first describes the assimilation of IMPROVE data using the
GEOS-Chem adjoint to create estimates of black and organic carbon
emissions, and then discusses how the resulting emissions are
partitioned by sector and analyzed for trends.

\subsection{Assimilation of IMPROVE Data with the 
GEOS-Chem Adjoint}
\label{assimilation of improve with gcadj}

\subsubsection{IMPROVE Data Preparation and Gridding}
In order to execute the assimilation of IMPROVE observations with the
GEOS-Chem adjoint, a direct comparison must be made between the
monitoring network's observations and the model's analogous
predictions. This is effected by first putting the IMPROVE data into
the GEOS-Chem grid by averaging sites in each GEOS-Chem grid-cell to
make it palatable for the cost function, and then performing
assimilation with the GEOS-Chem adjoint allowing emissions of black
carbon and organic carbon to vary in order to minimize the cost function.

IMPROVE data was accessed from the IMPROVE network's website, where
raw and processed data files are available \parencite{improvewebsite}. For
each year assimilations were performed, IMPROVE data were retrieved
from November 20th of the previous year to March 8th the year in
question, providing a small buffer before and after the December 1st
to February 28th assimilation. For each day in the retrieval period
where IMPROVE data exists, values for the total elemental carbon
aerosol, total organic carbon aerosol, and the constituent components
from which they're derived (four organic carbon measurements, one
organic pyrolized carbon measurement, and three elemental carbon
measurements), as well as uncertainty and minimum detection limits for
each were retrieved.

Because uncertainty and minimum detection limit information are not
provided for total elemental or total organic carbonaceous aerosol
quantities, they were calculated on the basis of the information
provided for the constituent quantities. As documented in the IMPROVE
data guide, total elemental carbonaceous aerosol and total organic
carbonaceous aerosol are calculated as follows
\begin{align}
\textrm{Total Elemental} &= EC1 + EC2 + EC3 - OP \nonumber \\
\textrm{Total Organic} &= OC1 + OC2 + OC3 + OC4 + OP
\end{align}
Using the standard equations for calculating standard error, we
calculated errors using
\begin{align}
\Delta \textrm{Total Elemental} &= \sqrt{
\Delta EC1^2 +\Delta EC2^2 +\Delta EC3^2 +\Delta OP^2
} \nonumber\\
\Delta \textrm{Total Organic} &= \sqrt{
\Delta OC1^2 +\Delta OC2^2 +\Delta OC3^2 +\Delta OC4^2 + \Delta OP^2
}
\end{align}
For minimum detection limits on derived quantities, we adopted the
maximum value among the components which comprise it. Values deemed
invalid by IMPROVE pre-processing (due, for example, to contamination,
filter clogging, or laboratory mishap) were simply discarded. Values
below the minimum detection limit were replaced with a value of half
the detection limit: without apriori knowledge, we assume low values
to be uniformly distributed between 0 and the minimum detection limit.
Discarding these data points would cause the final result to be biased
high.

One concern in using data from in-situ measurements is that they may
become contaminated by local sources. Because the IMPROVE sites are
located (for the most part) in remote locations, traffic related
events or trends that are highly local aren't expected to be important
\cite{murphy2008}. Outlier were eliminated by analyzing each IMPROVE
site on the basis of an expected log-normal distribution of
concentrations \cite{murphy2008}. Very few points (less than 0.1\%)
needed to be eliminated. Once the IMPROVE data have been confirmed to
conform to the expected statistics, the IMPROVE sites from each
grid-cell in the GEOS-Chem 4x5 grid are averaged to create a value to
be the input of cost function described in section \ref{cost function
  description}.


\subsubsection{Cost Function}
\label{cost function description}

As described in section \ref{adjoint method}, we seek to minimize a
cost function which measures the discrepancy between IMPROVE
observations and the GEOS-Chem simulation of analogous quantities. To
that end, we define our cost function as

\begin{equation}
\label{cost function}
J = \sum_t \sum_k \left[
\left(BCPI_{t,k} + BCPO_{t,k} - EC \right)^2 + 
\left(OCPI_{t,k} + OCPO_{t,k} - OC \right)^2
\right]
\end{equation}
where
\begin{defineterms}
$J$ & cost function \\
$t$ & iterator over model time steps \\
$k$ & iterator over model grid-cells \\
$BCPI$ \& $BCPO$ & GEOS-Chem black carbonaceous aerosol tracers\\
$OCPI$ \& $OCPO$ & GEOS-Chem organic carbonaceous aerosol tracers\\
$EC$ & Average black carbon of IMPROVE sites\\
$OC$ & Average organic carbon  of IMPROVE sites\\
\end{defineterms}
This cost function increases as the discrepancy between modelled and
observed black carbonaceous aerosol increases. It allows for spatial
variation and temporal variation with the winter season which is
expected \parencite{murphy2011} and as such should not be penalized.

The cost function was validated by performing a finite difference
test, which compares the cost function gradient calculated with the
adjoint method to an estimate obtained by dividing the perturbation in
cost function caused by a small change the control parameters (a
finite, i.e. not infinitesimal, difference).
\begin{equation}
\nabla_u J \approx \frac{J(u + \delta u) - J(u)}{\delta u}
\end{equation}
In order to expeditiously make this comparison across the model's
entire domain, transport processes (which have not been altered form
their thoroughly validated canonical state) are disabled, isolating
both the adjoint calculation and the finite difference process in each
of the model's grid-cells \cite{henze2007}. Figure \ref{fd figure}
shows finite difference tests showing the correctness of the adjoint's
gradient calculation for a number of GEOS-Chem's carbonaceous aerosol
related control variables.

\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth]{figs/finite_diff.png}
\caption{Finite difference tests for (clockwise from top-left)
  BCPI\_an, BCPO\_bf, OCPI\_bf, and OCPO\_an.}
\label{fd figure}
\end{center}
\end{figure}

To ensure that assimilated emissions have been effectively optimized
we can examine the evolution of the cost function through successive
iterations of optimization. Figure \ref{convergence figure} shows the
evolution of the cost function through successive iterations as a
fraction of their initial value; it can be seen that it stabilizes
before the model terminates.

Secondly, to guard against the possibility of the model arriving at a
local minimum and terminating before optimal emissions have been
found, assimilation can be performed with the same observations with
different starting conditions. This was done using normally
distributed initial scale factors with a mean of 1.0 (no change) and a
standard deviation of 0.5 for all variables being optimized. The
results were found to be insensitive to the initial scale factors.


\subsubsection{Optimization}

Data assimilation was performed for 1990, 1995, 2000, and 2005. In
each case, gridded IMPROVE observations were assimilated from December
1st of the previous year to February 28th. For computational
expediency, only carbonaceous aerosol tracers were included in the
simulation. 

Scale factors for emissions of black and organic carbon were allowed
to vary for anthropogenic (i.e. fossil-fuel related) and bio-fuel
emissions of carbonaceous aerosol. As described in section
\ref{current understanding of emissions}, biomass burning is expected
to be negligible for this season, and as such was deactivated. In each
iteration, the gradient of the cost function with respect to each
scale-factor was used to modify BC and OC emissions; the model was
then re-run with the modified emissions and the process repeated up to
a maximum of nine iterations--long enough for the cost-function
reduction to stabilize.

\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth]{figs/cf_evo.png}
\caption{Evolution of the cost-function over successive iterations}
\label{convergence figure}
\end{center}
\end{figure}

The final product is assimilated emissions of black carbon aerosol and
organic carbon aerosol in the GEOS-Chem 4x5 grid for the winter months
of each simulation year.


\subsection{Trend Analysis and Sectoral Partitioning}
\label{trend analysis}
Though GEOS-Chem separates carbonaceous aerosol emissions by sector,
the adjoint attempts to minimize the cost function by varying chemical
species rather than emission sectors. For this reason, there is no
expectation that assimilated emissions preserve sectoral
distributions. In order to recover sectoral information from the
assimilated emissions we adopt the following simple model.

In the winter months in North America, we expect only emissions from
fossil fuel use ($E_\textrm{anth}$) and bio-fuel use
($E_\textrm{biof}$) to be important contributors to carbonaceous
aerosol \parencite{bond2004}. Furthermore,
we assume that within a sector, the ratio of black to organic carbon
is constant, so that if the ratio of black or organic carbon is known,
the emissions of either species can be recovered from the total
emissions.
\begin{align}
r_a &\equiv \frac{\bcan}{\bcan + \ocan} \nonumber \\
r_b &\equiv \frac{\bcbf}{\bcbf + \ocbf}
\end{align}
where
\begin{defineterms}
$ \bcan $ & Fossil-fuel black carbon \\
$ \bcbf $ & Bio-fuel black carbon \\
$ \ocan $ & Fossil-fuel Organic Carbon \\
$ \ocbf $ & Bio-Fuel Organic Carbon \\
\end{defineterms}
%
With these assumptions, the relationship between
emissions in each sector and the total emitted black or organic
carbonaceous aerosol can be calculated thus:
%
\begin{align}
\label{emissions from ratios}
r_a E_a + r_b E_b = BC \nonumber \\
(1-r_a) E_a + (1-r_b) E_b = OC
\end{align}\\
%
where $BC$ and $OC$ are the total emissions of black and organic
carbonaceous aerosol respectively. Even though the model's sectoral
distribution is compromised by the optimization routine, {\it total}
emissions of each species should be effectively optimized by the cost
function, so $BC$ and $OC$ can be reliably calculated from the
assimilated emissions. For the present study, ratios were calculating
using the emissions inventory of Cooke, the same inventory as used in
the GEOS-Chem simulations \citeyear{cooke1999}.


Assuming that $r_a$ and $r_b$ remain constant throughout the
simulation period, we can differentiate equation \ref{emissions from
  ratios} 
\begin{align}
r_a \dot{E_a} + r_b \dot{E_b} = \dot{BC} \nonumber \\
(1-r_a) \dot{E_a} + (1-r_b) \dot{E_b} = \dot{OC}
\end{align}
where $\dot{x} = \frac{dx}{dt}$. By calculating $\dot{BC}$ and $\dot{OC}$
from the assimilated emissions, we create a system of two equations
with two variables ($\dot{E_a}$ and $\dot{E_b}$) in each grid-cell which can be
solved to find a combination of trends in total fossil-fuel and
bio-fuel emissions which reproduce the observed trend in carbonaceous
aerosol concentrations while maintaining a consistent $\frac{BC}{OC}$
emission ratio in each sector.
