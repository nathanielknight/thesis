\section{Results}
This section describes the results of the assimilation described in
section \ref{method}. Assimilated emissions and trends in black and
organic carbonaceous aerosol are shown, followed by emissions and
trends divided into fossil-fuel and bio-fuel related activity as
described in section \ref{trend analysis}. 


\subsection{Assimilated Concentrations}


Figure \ref{bc conc fig} shows prior and posterior black carbon
concentrations for the winter months of 2002. Because the GEOS-Chem
adjoint doesn't realistically assimilate sectoral emissions (indeed,
it's unclear that such an assimilation would be possible with the
current understanding of emission factors), emissions are aggregated
from all GEOS-Chem sectors to avoid spurious sectoral information.
GEOS-Chem grid cells over ocean (where BC emissions are exclusively
from ship emissions beyond the scope of this analysis) are excluded.

The assimilation drives down emissions (and thus concentrations) even
though some IMPROVE sites show BC concentrations in line with the
model's a-priori representation. The decrease is most pronounced on
the West Coast. Since prevailing winds are West-to-East, emissions in
this region influence concentrations well beyond their own cells.

\begin{figure}[htb]
\includegraphics[width=0.9\textwidth]{figs/bc_assimilation.png}
\caption{
  Average BC Concentration from December 2001 to February
  2002
}
\label{bc conc fig}
\end{figure}

Figure \ref{oc conc fig} shows similar quantities for OC. Despite some
regions being clearly disparate (sites near the Mexican border, for
example), this assimilation is less dramatic. Average IMPROVE
concentration within each GEOS-Chem grid cell (which may contain
several sites) is more consistent with the prior concentration.
Concentrations of OC are also up to ten times higher than BC, leading
to smaller relative errors, and a smaller relative importance for
similar changes in concentration.

There is inter-annual variability in both black and organic carbon, so
these patterns are by no means characteristic. Complete assimilations
are available.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\textwidth]{figs/oc_assimilation.png}
\caption{
  Average OC Concentration from December 2001 to February 2002
}
\label{oc conc fig}
\end{center}
\end{figure}


Analysis of trends in the IMPROVE BC and OC concentrations has been
carried out by Murphy et. all \parencite{murphy2011}. The primary
candidates for changing concentrations in the United States are
wood-burning stoves and diesel trucks and off-road equipment.
Emissions standards have been introduced for both of these sources,
but we expect a gradual response due to slow turnover of equipment and
increased fuel consumption. Although downward trends of black and
organic carbon were found to be correlated, the decrease was not
correlated with sulphate, suggesting that sulphate heavy sources (such
as coal-fired power plants) are not the culprit. 

\subsection{Assimilated Emissions}
\label{assimilated emissions}
Figures \ref{bc trend fig} and \ref{oc trend fig} show the trend in
assimilated emissions of black and organic carbonaceous aerosol,
described as an annual percentage of the average over the same period
to prevent outlying concentrations in a particular year from having a
disproportionately large influence. In order to exclude potentially
spurious results arising from modest changes in grid-cells with small
emissions to begin with, only grid-cells which had at least 0.1 Gg of
carbonaceous aerosol emission are considered.

As expected from the trends in the IMPROVE network's observations (see
section \ref{improve network}), we find decreases in emission of black
and organic carbonaceous aerosol. The two most significant areas of
decrease are the West Coast and Great Lakes/New England region. This
is consistent with trends in the IMPROVE data and expectations based
on emissions controls.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\textwidth]{figs/bc_trend.png}
\caption{
  Trend in assimilated emissions of black carbonaceous aerosol in each
  GEOS-Chem grid-cell from 1992-2004. Units are annual change as a
  percentage of average emissions over the same period.
}
\label{bc trend fig}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\textwidth]{figs/oc_trend.png}
\caption{ Trend in assimilated emissions of organic carbonaceous
aerosol in each GEOS-Chem grid-cell from 1992-2004. Units are annual
change as a percentage of average emissions over the same period.}
\label{oc trend fig}
\end{center}
\end{figure}

Some GEOS-Chem cells in Canada (some distance from any IMPROVE
stations) show strong, opposing trends, an unexpected result. One
possibility is that unavoidable errors biomass burning inventories (a
major source of black and organic carbon at these latitudes) have
introduced extra aerosol that the analysis doesn't account for. It's
also possible that a quirk of transportation makes shifting emissions
particularly favourable to the assimilation, especially at this coarse
resolution. In any case, Emissions of both black and organic carbon so
far north of the border tend to be marginal (~5\% of typical emissions
in the continental US), and these changes aren't expected to influence
our conclusions.

There is a large variation in the standard error ($\approx$ 10\% to
more than 300\%) and p-values of these trends, with regions without
IMPROVE stations showing more uncertainty.

With the exception of a few cells in California and Nevada where
significance requirements are not met, 

The trend in OC emissions for the West Coast is even stronger in the
assimilation than in the IMPROVE data while downwind sites show little
or no decrease. This highlights the potentially long-ranged nature of
the influences on carbon aerosols despite its relatively short
atmospheric lifetime \parencite{bond2013, liu2011}.

Intriguingly, both BC and OC increase in Florida and the Caribbean
Islands, a trend which isn't discussed in the literature. As black
carbon is a powerful greenhouse agent, determining the source of these
emissions and mitigating it could be an effective regional climate
change strategy.


\subsection{Trends in Sectoral Emissions}
By combining observed trends with information about the ratio of black
and organic carbon emitted by different sectors, we can apportion the
changes in black and organic carbon concentrations to changes in
emissions from specific sources.


Figure \ref{anth trend} shows the assimilated trend in
winter-time (DJF) fossil-fuel emissions for the period from 1992 to
2004. Figure \ref{biof trend} shows the assimialted trend
in winter-time bio-fuel emissions for the same period. Emissions are
shown as annual change as a percentage of the average for the same period.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/anth.png}
\caption{Assimilated trend in winter fossil-fuel emissions from 1992
  to 2004 as a percentage of the average for the same period.}
\label{anth trend}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/biof.png}
\caption{Assimilated trend in winter bio-fuel emissions from 1992 to
  2004 as a percentage of the average for the same period.}
\label{biof trend}
\end{center}
\end{figure}


Ratios of black and organic carbon for each sector were calculated
based on the inventory of Bond et. al. \parencite{bond2004}, the model
used for anthropogenic and biofuel emissions over North America in the
GEOS-Chem model. Despite all possible care being taken in its
construction, a great deal of uncertainty is unavoidable in the
inventory, as it relies on imperfect knowledge of fuel consumption and
even more imperfect knowledge of how fuel consumption translates into
emissions.

Our analysis indicates that fossil-fuel BC decreased by a few percent
a year throughout most of the United States with the exception of
California, where it increased by up to 4\%. Bio-fuel use dropped
sharply (by up to 14\%) in California and the mid-west but increased
in the prairies, though some trends weren't significant in that
region. This is a result of the decrease in OC being stronger than BC,
as OC makes up a greater fraction of bio-fuel emissions. Another
interpretation would be that fossil-fuel emissions have more OC than
we'd expect, either through changing emission factors (decreasing BC
from emission controls, for example) or through secondary-organic
aerosol mechanisms not accounted for in the inventory's treatment of
fossil fuel emissions. These alternative explanations are supported by
the literature, as we'll see.

Emissions of black and carbonaceous aerosol emitted from fossil-fuel
consumption in transportation varies greatly depending on vehicle
type. Diesel vehicles in particular seem to emit a disproportionately
large amount of black carbon, though medium- and light-weight trucks
have BC/OC profiles somewhere between the low-BC emissions of gasoline
vehicles and the high-BC emissions of heavy
diesel \parencite{dallmann2013}. Furthermore, secondary organic aerosol
is thought to be an important component of diesel emissions
\parencite{gentner2012}, but is poorly represented in GEOS-Chem and
our analysis. Correcting emissions factors or improving our
understanding of SOA could shift the BC/OC ratio for fossil fuels,
effectively redistributing emissions from bio-fuel to fossil-fuel in
our analysis.

Ahmed et. al. conducted an in-depth study of the evolution of BC
concentrations in Mayville, NY \parencite{ahmed2014}. They describe
difficulties in determining the contribution of local traffic to BC,
citing the changing emission factors of diesel vehicles and the
uncertain composition of traffic. Additionally, by comparing the
optical properties of BC at different wavelengths using a two-channel
aethlometer they estimate that biofuel burning contributed 28\% of the
BC at their site during the winter months. They report a decrease of
32\% in the concentration of BC betweeen 1984 and 2010. The decrease
in overall BC is consistent with our findings, but our analysis
indicates much stronger decreases in bio-fuel than fossil fuel in the
region. That a source responsible for approximately a third of total
BC should completely disappear (as it would have to to produce the
observed change in concentration) is surprising, and suggests that a
change in the sectoral distribution of BC and OC in the region may be
in order.

Another possible changing source of EC emissions is described by Jeong
et. al. \parencite{jeong2013} : factor analysis at sites in southern Ontario found an
un-described source contributing up to 50\% of the total black carbon
in the region. The source was inconsistent with local diesel emissions
as it was equally represented in both high-traffic and low-traffic
areas. The corresponding concentrations of OC aren't discussed (though
the BC in question was thermally stable, suggesting efficient
combustion);the geographic source was found to be Ohio and Western
Pennsylvania, home to heavy industry and coal-fired power plants. Our
analysis shows a decrease organic carbon for the region, interpreted
as a sharp decrease in bio-fuel use. Another explanation could be
changes in an un-characterized industrial source.

On the West Coast, Junker et. al. analyzed \parencite{junker2004}
analyzed the abundance of BC at a site in New Mexico, finding black
carbon decreased from 0.16 to 0.11 $\mu$g between 1990 and 1995 (an
amount which is consistent with our decrease, though the periods only
overlap for three years). However, Junker et. al's decrease had no
seasonal dependence, which is at odds with our finding that bio-fuel
use decreased substantially in the region. Junker et. al.'s
back-trajectory analysis found that their site's air became
particularly loaded with BC after passing over urban centers;
additionally, as it is only a single site, it's possible that it
happens to lie outside the influence of bio-fuel source. It does,
however, not support our finding of an increase in fossil-fuel BC on
the west coast, indicating that a change of BC/OC ratio would lead to
a better reconstruction.

Attributing changes in emissions to a particular sector requires
accurate knowledge of the ratio of black and organic carbon typical
for that sector. This ratio is both difficult to calculate, and
potentially highly variable due to the disparate nature of consumption
which is nevertheless grouped in a single category \cite{cooke1999}.
Another possibility is that changing technology or consumption
patterns within a sector have caused the distribution of black and
organic carbon to shift, a contingency for which the present analysis
makes no allowance.
