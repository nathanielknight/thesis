#!/bin/bash

clear

sourcefiles=$(ls | grep "[0-9].*.tex")
echo $sourcefiles

echo "Todos: --------------------------------------------------------------------"
grep -n todo $sourcefiles

echo
echo "Citations Needed: --------------------------------------------------------"
grep -n citationNeeded $sourcefiles
