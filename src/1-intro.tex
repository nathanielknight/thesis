\section{Introduction}

Carbonaceous aerosols in the atmosphere can be loosely divided into
two categories: organic materials and black carbon (or ``elemental'',
``light-absorbing'', or ``free'' carbon). While organic carbonaceous
aerosols can be emitted directly or produced by the reaction of
precursor gasses, black carbon is formed during the process of
combustion, and therefore has only primary emissions
\parencite[Chapter~2]{seinfeld-and-pandis}. The practical interest in
carbonaceous aerosol comes principally from two domains: climate
change and air quality.

Black carbonaceous aerosol is a powerful absorber of visible light,
with positive radiative forcing due directly to absorption while
aloft, positive forcing caused by blackening as it is deposited on the
Earth's surface (especially in the arctic), and possible negative
forcing due to effects on clouds. Because they are concentrated at the
equator (where solar irradiance is highest), and can mix with other
aerosols to form absorptive brown clouds, emissions of black carbon
aerosol are the second most important contribution to global warming
after CO$_2$\parencite{ramanathan2008}; there is evidence that the
magnitude of its effects in this capacity are at preset dramatically
underestimated \parencite{bond2013}. Evidence also suggests that black
carbon forcing is particularly important in the arctic \parencite{sand2013}.

Black carbon is a component of PM$_{2.5}$, with documented negative
health impacts \parencite{boldo2006}. Moreover, black carbon aerosol
in particular has been linked to negative health
impacts \parencite{janssen2011}; links have been made between black
carbon and lung cancer \parencite{knaapen2004}.

Despite its importance in these domains there are a number of
unresolved questions about the provenance of carbon aerosols in the
atmosphere. Sources of black carbon (biomass-burning, bio-fuel use,
and fossil-fuel use) are difficult to quantify and variable in the
amount and type of carbon aerosol they emit \parencite{chung2002}.
Additionally, a large number of organic compounds contribute to
primary and secondary organic carbonaceous aerosol, the sources,
abundances, and chemical mechanisms of which are not well
understood \parencite{atkinson2003}. This includes the formation of
organic carbonaceous aerosol from biogenic trace gasses
\cite{griffin1999} and an evolving understanding of the contributions
of transportation-related fossil-fuel use to organic
aerosol \parencite{gentner2012}. The large number of uncertain
contributing factors makes it difficult to attribute changes in
atmospheric concentrations of carbonaceous aerosol to changes in
particular emissions. Beyond the purely scientific pursuit of better
understanding the sources and interactions of aerosols in the
atmosphere, understanding black carbon is an important step in
mitigating its impact, which has been proposed as an approach to
combating climate change \parencite{bond2005}.

One such unresolved trend was captured by the Interagency Monitoring
of Protected Visual Environments aerosol monitoring network (IMPROVE),
which operates in North America. Over the course of its 26-year
operation (1988-present) it has recorded a steady decline in the
winter-time concentration of carbonaceous aerosols throughout the
continental United States, especially on the West coast. The cause of
this decline is unknown, but candidates include changes in
transportation and industry emissions caused by new emissions
standards, or changes in the use of bio-fuels \parencite{murphy2011}.

The present study implements a method for combining observations of
aerosol concentrations with information about the emission, transport,
and removal of aerosols embodied in a chemical transport model. Its
goal is to generate enhanced estimates of winter-time carbonaceous
aerosol emissions in the United States partitioned by source sector.

The next chapter presents the IMPROVE monitoring network in more
detail, describes the adjoint method as it applies to data
assimilation, and its implementation in the GEOS-Chem adjoint. The
following chapter documents the present study's approach to
assimilating IMPROVE data into the GEOS-Chem emissions inventory and
steps that were taken to validate its implementation. Subsequent
chapters present the results of that assimilation, discuss their
implications, and delineate future avenues of work.
