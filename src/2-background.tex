\section{Background}

To begin, this chapter presents an overview of the current approaches
to understanding carbonaceous aerosol emissions. To follow, there are
sections presenting the GEOS-Chem global chemical transport model, the
IMPROVE aerosol monitoring network, the adjoint method of data
assimilation, and the implementation of the GEOS-Chem Adjoint. These
represent the starting point of the present study, the ingredients of
the analysis, and the means by which they will be combined to address
the problems presented in the Introduction.

\subsection{Carbonaceous Aerosols}
\label{current understanding of emissions}

Carbonaceous aerosol refers to particulate matter in the atmosphere
composed of organic compounds. Is it generally divided into two
categories. Black carbonaceous aerosol is a product of combustion,
with fuel use (especially coal) and open-burning being the principal
sources. Organic carbonaceous aerosol is co-emitted during combustion
with black carbonaceous aerosol, but is also emitted from natural
sources. Plant activity can directly produce organic aerosols or emit
precursor gasses which then transition into the solid phase
\parencite{park2003, liousse1996}. 


\subsubsection{Emissions Inventories}
Current approaches to understanding carbonaceous aerosol emissions
adopt one of two approaches. Bottom-up inventories combine
the properties of fuels and combustion with activity statistics to
estimate emissions; these approaches focus on anthropogenic emissions
from fuel consumption. For open-burning (the other major source of
carbonaceous aerosol), top-down inventories infer emissions
based on remote sensing.

Key considerations in estimating black carbon emissions from fuel
consumption are fuel consumption rates and emission factors (i.e. how
much black carbon is emitted per unit of fuel consumed). Emission
factors depend on such variables as combustion type, fuel type, and
the qualities of technology in the region where it is consumed;
emissions statistics indicate that black carbon emission factors for
the same application can vary by an order of magnitude or more based
on technology differences \parencite{cooke1999}. These inventories account
for consumption of fuels such as coal, petroleum, natural gas, peat,
and wood.

Cooke et. al. describe one such inventory based on UN fuel consumption
data; they find North America to contribute 0.55 Tg of black carbon to
a global of 6.39 Tg in 1984 \parencite{cooke1999}. Another such inventory
is described in \parencite{bond2007} in which the dominant source of black
carbon changes throughout the 20th century with economic,
technological, and population pressures, and has at different times
been bio-fuel use, coal, and diesel. The dominant region has also
shifted from North America to Asia \parencite{bond2007}. 

Open burning has been characterized in the Global Fire Emissions
Database, which estimates emissions from open burning based on global
maps of burned area, fire activity, and plant productivity derived
from satellite observations. It finds the the largest contributor to
open-burning emissions of carbonaceous aerosol to be fires in
grasslands and savannas, with smaller contributions from tropical
deforestation burning and woodland fires (mostly confined to the
tropics). In the winter months, biomass burning in North America is
negligible \parencite{vanderwerf2010}.


\subsection{The IMPROVE Monitoring Network}
\label{improve network}
The IMPROVE aerosol monitoring network has over 70 monitoring stations
across North America with observations dating back to 1989. It
collects aerosol for one day out of every three, analyzing the
abundance and composition of aerosol collected over a 24 hour period.
Sites are located in remote areas in order to minimize the localized
effects of transportation or industrial emissions, yielding
measurements that represent the background concentration of aerosol in
the atmosphere \parencite{malm1994}.

Carbonaceous aerosol information is obtained from the analysis of
quartz filters by the Thermal Optical Reflectance (TOR)
method \parencite{chow2007}. The reflectance of the sample and the
CO$_2$ evolved is measured while it is heated in stages in an
atmosphere of helium. When the sample reaches 550 $^oC$, 2\% oxygen
gas is added to its atmosphere and heating continues. The sample's
reflectance decreases during the initial, oxygen-free heating,
returning to its original value and then increasing as the oxygen is
added. Any carbon evolved during the initial, oxygen-free heating is
considered organic. The carbon which evolves after the addition of
oxygen but before the sample's reflectance has returned to its
original value is considered to be pyrolized organic carbon produced
during the initial heating process (and is thus added to the OC
content of the sample). Any subsequently evolved carbon is considered
light-absorbing.

Hardware changes in 2005 introduce potential discontinuities in the
time series. Though no change in total carbon were observed, the ratio
of black carbon to organic carbon was not consistent across the
upgrade, and we do not use observations beyond
2005 \parencite{chow2007, white2008}.

The IMPROVE network observed a uniform decrease in total carbonaceous
aerosol between 1989 and 2008 during the Winter months (December,
January and February). The most pronounced decrease was along the West
Coast, with changes of up to 10\% per year. The East Coast also saw a
significant decrease, but with typical trends no higher than 2.5\% per
year \parencite{improvereportV}. (See section \ref{assimilated
  emissions} for visualizations of the IMPROVE trends and comparison
to our results).

Between 1995 and 2004, the IMPROVE network observed a substantial
decrease (>50\% over 10 years) in black carbonaceous aerosol along the
West Coast. A less pronounced (25\% to 50\% over 10 years) trend was
observed on the East Coast and in the Ohio River Valley. Organic
carbon underwent a similar decrease on the West Coast, but was
effectively steady on the East Coast, even increasing in the
Carolinas, Georgia, and along Florida's Atlantic coast
\cite{improvereportIV}.


\subsection{The GEOS-Chem Model}
\label{geos chem model}

GEOS-Chem is a global chemical transport model using the Goddard Earth
Observation System's assimilated meteorology. It describes the
chemistry and advection of approximately one hundred atmospheric
chemical species or tracers (representing aerosols or families of
related chemical species). These species are related by a few hundred
chemical reactions, including photo-chemistry and aerosol
thermodynamics \parencite{bey2001}. 

GEOS-Chem's representation divides carbonaceous aerosols with two
distinctions: black carbon (BC) vs. organic carbon (OC) and
hydrophobic vs. hydrophilic, for a total of four chemical tracers. In
addition to the distinctions between aerosol species, carbonaceous
aerosols are divided by their sector of emission, with anthropogenic,
biomass-burning, and bio-fuel contributions for black and organic
carbon, and an additional biogenic source for organic carbon only
\parencite{cooke1999}. Hydrophobic carbon ages to hydrophilic carbon as it
is oxidized and mixes with inorganic aerosol, represented in the model
with an e-folding time of 1.2 days \parencite{chin2002}. Wet and dry
deposition schemes include scavenging in convective updrafts, rainout
from convective anvils, and rainout and washout from precipitation
\parencite{liu2001}.

When dealing principally with aerosol, GEOS-Chem can be run with
off-line chemistry, using archived concentrations of relevant
atmospheric tracers (OH, NO$_3$, O$_3$, and total nitrate) and
production/loss rates (H$_2$O$_2$), bypassing a portion of the
chemistry and thermodynamics routines \parencite{gchemofflineaerosol}.
Because Carbonaceous aerosols are mostly inert, we expect carbonaceous
aerosol emissions to have little impact on the relevant archived
tracers. As such, we run the model in offline aerosol mode for
simplicity and computational expediency. \citationNeeded{ author of
  GC-Adj offline aerosol}

In GEOS-Chem, the carbonaceous aerosol emissions associated with human
activity are based on the inventories of \parencite{cooke1999} and
\parencite{bond2007} described in section \ref{current understanding of
  emissions}. The Cooke inventory (used in GEOS-Chem for North
America) is based on fossil-fuel consumption statistics published by
the UN with emission factors that vary according to the type of
consumption (domestic, industrial, transportation) and a region's
estimated level of technological development \parencite{cooke1999}.
The Bond inventories are compiled by country, and are based on fuel
consumption statistics from the UN and IEA with modification based on
the type of technology present in each nation (or, when data is
unavailable, in similar nations that have been characterized)
\parencite{bond2007}. Both inventories have substantial uncertainties.


\subsection{The Adjoint Method}
\label{adjoint method}

Variational data assimilation is a method for combining observation
and simulation in a manner that preserves physical consistency in the
model. It emerged in the context of numerical weather prediction as a
means of improving forecasts by modifying predictions with subsequent
observations \parencite{talagrand1997}. It aims to adjust {\it control
  variables} in the model in order to minimize a {\it cost function}
measuring the discrepancy between observations and corresponding model
predictions. It has also been applied in atmospheric chemistry and
dynamic oceanography. A typical cost function for data assimilation is
\begin{equation}
J(v) = \left<
v-v', v-v'
\right>
\end{equation}
where $v'$ and $v$ represent a set of observations and the analogous
simulations of the same quantity, respectively. Minimizing this cost
function minimizes the discrepancy between analogous simulated and
observed quantities. Efficient cost function minimization is possible
if the cost function's gradient with respect to the model's control
variables is known. The adjoint method is a means of efficiently
calculating this gradient \parencite{talagrand1997,wang2001,giering1998}.

The adjoint method can be understood by considering the model as a
vector mapping $\mathbf{L}$ from a vector of model control parameters
$u$ to model predictions $v$.
\begin{equation}
v = L(u)
\end{equation}
Here, $u$ and $v$ are (finite dimensional) vectors of real numbers of
appropriate size for the control parameters and model output. The
adjoint approach requires $u$ and $v$ to be members of {\it Hilbert
  space},
\footnote{A Hilbert space is a complete vector space with an inner
  product allowing length and angle to be measured. This definition is
  a good deal more general than what we require for the present
  application, but is included for the sake of consistency with other
  sources in the literature.}
a requirement which our current assumptions satisfy.


We seek to minimize a cost function
\begin{equation}
J(u) = \frac{1}{2} \left<
L(u)-v', L(u)-v'
\right>
\end{equation}
measuring the difference between observations $v'$ and model
predictions $v=L(u)$ of the same quantity. In order to efficiently
minimize the cost function, we seek to calculate the gradient of $J$
with respect to model parameters, $\nabla_u J$. For the simplicity of
this exposition, we'll further suppose that $J$ is sufficiently linear
to be represented by a first order linear expansion, and that $L$ is
sufficiently linear that the variation $\delta v$ in model predictions
engendered by a variation $\delta u$ in the model's control parameters
may be represented by a linear operator
\begin{equation}
\delta v = A(u_0) \delta u
\end{equation}
depending on the control parameters' initial value.

With these assumptions, we can expanding $J(u)$ around some initial
control factors $u_0$ to get 
\begin{equation}
J(u) = J(u_0) + \left<\nabla_u J(u), u-u_0\right> + o|u-u_0|
\end{equation}
or in terms of variations
\begin{equation}
\delta J = \left< \nabla_u J(L(u)), \delta u \right>
\end{equation}

A result from calculus on Hilbert spaces (e.g. vectors of real
numbers, as we'll consider in our model) is that for every continuous
linear operator $L$ mapping $u$ to $v$ there exists a unique
continuous operator $L^*$ such that
\begin{equation}
\left<Lu,v\right> = \left<u,L^*v\right>
\end{equation}
where $\left<*,*\right>$ indicates the inner product \parencite{wang2001}.
In the case of vector spaces over real numbers, $L^*$ is the transpose
of $L$.

Differentiating the cost function defined above with the product rule,
making linear approximations where necessary, and applying the
symmetry of the inner product, we find
\begin{align}
\delta J & = \left<
  \delta v, v-v'
\right> + \left<
  v-v', \delta v
\right> \nonumber \\
& = 2 \left<
  v-v', A(u_0)\delta u
\right>
\end{align}
Finally, applying the definition of the adjoint operator,
\begin{equation}
\delta J = 2 \left<A^*(u_0) (v-v'), \delta u\right>
\end{equation}
we obtain an equation that we can compare to our definition of the
gradient to see that 
\begin{equation}
\nabla_u J = A^*(u_0) (v-v')
\end{equation}
\parencite {giering1998}.

Naturally, the implementation of adjoint code is not performed with
matrix transposes and model control and state described in flat
vectors. By considering our model $L$ as a series of discrete steps
\begin{equation}
L = L_0 \cdot L_1 \cdot L_2 \cdots L_n
\end{equation}
we can construct the adjoint for each step individually. Recalling
that the adjoint of real linear operators is the transpose, 
\begin{align}
L* &= (L_0 \cdot L_1 \cdot L_2 \cdots L_n)^T \nonumber \\
&= L_n^T \cdots L_2^T \cdot L_1^T \cdot L_0^T
\end{align}
\parencite{giering1998}. Descriptions of how to construct $L^*$ from
$L$ in terms of computational operations instead of matrices have been
described and, to some extent, automated \parencite{giering1998}.
Nevertheless, the construction of an adjoint model from forward-model
code is no small task, requiring expertise and careful attention to a
great many details. For our task of studying black carbon, we use an
existing adjoint model derived from an existing global chemical
transport model \cite{henze2007}.

The adjoint method, though 2.5 to 4.5 times slower than the forward
models they are derived from, represent a very efficient method of
optimization \parencite{giering1998}. A five-times run-time increase is
unquestionably more feasibly than performing a forward run for
each control parameter in each of a model's grid-cells perturbed
individually.


\subsection{The GEOS-Chem Adjoint and Data Assimilation}
\label{geos chem adjoint}
The adjoint of GEOS-Chem is complementary to the forward model, and
implements its adjoint. It was initially developed with a focus on
sulphate - ammonia - nitrate chemistry and transport, but includes the
mechanisms (notably emission) that govern carbonaceous aerosols as
well. It has been thoroughly tested and applied to several problems
similar to the one we presently consider \parencite{henze2007}.
\citationNeeded{offline aerosol development? adjoint thereof?}

In addition to providing the partial derivatives of a cost function, the
GEOS-Chem adjoint implements data assimilation. The gradient of a cost function
with respect to scaling factors $[\sigma_i]$ is calculated using the adjoint
method. This is provided to an optimization routine 
%{{{
\footnote{The optimizing routine used is the quasi-Newton LBFGS-B
  optimization routine.}%}}}
which iteratively seeks scaling factors that minimize the cost
function. With a cost function that measures the discrepancy between
modelled and observed values of the same parameters, this process
generates improved a-posteriori estimates of the parameters in
question \parencite{henze2009}. Scaling factors can be applied to a
number of model parameters (starting concentrations, rate constants of
chemical or physical processes, etc.); in our case they are applied to
emissions of carbonaceous aerosols. The GEOS-Chem adjoint has been
successfully for data assimilation, including the assimilation of
satellite observations to constrain global sources of mineral
dust \parencite{wang2012} and aerosols \cite{xu2013}. It has also been
used to perform sensitivity studies assessing the influence of
PM$_{2.5}$ precursor emissions on air quality in the United
States \parencite{henze2009} and tracing the origin of black carbon in
the remote interior of the Tibetan plateau \parencite{kopacz2011}.
